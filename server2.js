var express = require('express');
var app = express();

//path for static content
app.use('/static', express.static(__dirname + '/static'));

//setup ejs
app.engine('.html', require('ejs').__express);

//set views dir to /views
app.set('views', __dirname + '/views');

//automatic file extension
app.set('view engine', 'ejs');

//append for static content
app.set("path","/");

//the logger
app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  next();
});

app.get('/', function(req, res){
  console.log(app.get("path"));	
  res.render("index2",{
  	path: app.get('path')
  });
});

app.listen(3000);