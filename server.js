var app = require('http').createServer(handler)
  , url = require('url')  
  , fs = require('fs')
  , util = require('util');


app.listen(3000);
function handler (req, res) {
  console.log(req.url);
  if(req.url == "/" || req.url == "/index.html")
  {
    fs.readFile(__dirname + '/index.html',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading index.html');
      }
      res.writeHead(200);
      res.end(data);
    });
  }
  else if(req.url.indexOf("/current") != -1)
  {
    var url_parts = url.parse(req.url, true);
    var symb = url_parts.query.symb;
    getCurrentVolume(symb,res);
  }
  else
  {
    var url_parts = url.parse(req.url,true);
    console.log(url_parts.query.symb);
    //console.log(getQuote("LITL"));
    getQuote(url_parts.query.symb, res);
  }
}

function getQuote(symb, res)
{
  var exec = require('child_process').exec;
  exec("curl 'http://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/getHistoricalData.jsp?symbol="+symb+"&fromDate=&toDate=&datePeriod=3months&hiddDwnld=' -H 'Cookie: JSESSIONID=B1F24171A393784DB6359B21C4847399; NSE-TEST-1=1793073162.20480.0000; pointer=1; sym1=LITL' -H 'Accept-Encoding: gzip,deflate,sdch' -H 'Host: www.nseindia.com' -H 'Accept-Language: en-US,en;q=0.8,ms;q=0.6' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36' -H 'Accept: */*' -H 'Referer: http://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/getHistoricalData.jsp??symbol=LITL&fromDate=&toDate=&datePeriod=1month&hiddDwnld=true' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --compressed", function callback(error, stdout, stderr){
    //console.log("done");
    //console.log('stdout: ' + stdout);
    res.writeHead(200);
    res.end(stdout);
  });
}
function getCurrentVolume(symb, res)
{
  var exec = require('child_process').exec;
  exec("curl 'http://www.nseindia.com/marketinfo/companyTracker/ajaxquote.jsp?symbol="+symb+"&series=EQ' -H 'Accept-Encoding: gzip,deflate,sdch' -H 'Host: www.nseindia.com' -H 'Accept-Language: en-US,en;q=0.8,ms;q=0.6' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36' -H 'Accept: */*' -H 'Referer: http://www.nseindia.com/companytracker/cmtracker.jsp?symbol=L%26TFH&cName=cmtracker_nsedef.css' -H 'Cookie: NSE-TEST-1=1793073162.20480.0000; sym1=LITL; sym2=L&TFH; sym3=ARVIND; pointer=3; JSESSIONID=4B8F7A16EAB9543E5CA82DC7369D54AF' -H 'Connection: keep-alive' --compressed", function callback(error, stdout, stderr){
    //console.log("done");
    //console.log('stdout: ' + stdout);
    if(!stdout.match("No Data Found.")){
      var a1 = stdout.split(":");
      var len = eval(a1.length - 1);
      var ltp = a1[13];
      var buy_qty = a1[18];
      var buy_price = a1[19];
      var sell_price = a1[20];
      var sell_qty   = a1[21];
      var turnover   = a1[17];
      var volume = a1[16];
    }
    else
    {
      var ltp = "-";
      var buy_qty = "-";
      var buy_price = "-";
      var sell_price = "-";
      var sell_qty   = "-";
      var turnover   = "-";
      var volume = 0;
    }
    res.writeHead(200);
    res.end(volume);
  });
  
}